package com.devcamp.s50.jbr540.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.jbr540.api.model.Invoice;
import com.devcamp.s50.jbr540.api.service.InvoiceService;

@RestController
@RequestMapping("/")
@CrossOrigin(value="*",maxAge = -1)
public class InvoiceController {
    @Autowired
    private InvoiceService invoiceService;
    @GetMapping("/invoices")
    public List<Invoice> getInvoice() {
        List<Invoice> invoiceList = invoiceService.getInvoiceList();
        return invoiceList;
    }
}
