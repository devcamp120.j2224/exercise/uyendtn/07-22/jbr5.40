package com.devcamp.s50.jbr540.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.jbr540.api.model.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;
    public List<Invoice> getInvoiceList() {
        Invoice invoice1 = new Invoice(1, customerService.customer1, 10000);
        Invoice invoice2 = new Invoice(2, customerService.customer2, 15000);
        Invoice invoice3 = new Invoice(3, customerService.customer3, 20000);

        List<Invoice> invoiceList = new ArrayList<Invoice>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);
        return invoiceList;
            
        
    }
}
