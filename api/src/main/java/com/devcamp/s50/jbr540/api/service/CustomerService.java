package com.devcamp.s50.jbr540.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.s50.jbr540.api.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "OHMM", 10);
    Customer customer2 = new Customer(2, "XTRA", 20);
    Customer customer3 = new Customer(3, "UNIVERSAL", 15);

    public List<Customer> getCustomerList() {
        List<Customer> customerList = new ArrayList<Customer>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        return customerList;
    }
}
